﻿<%@ Page Title="Result Page" Language="C#" AutoEventWireup="true" CodeBehind="ResultPage.aspx.cs" Inherits="UPSRatingSample.Pages.ResultPage" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - UPS Rating API Sample</title>
    <link href="~/Content/Site.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <form runat="server">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" runat="server" href="~/">UPS Rating API Sample</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a runat="server" href="~/">Home</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container body-content">
            <h2>Result</h2>

            <asp:Repeater ID="dataGrid" runat="server">
                <HeaderTemplate>
                    <table class="table">
                        <tr>
                            <th>Service name
                            </th>

                            <th>Delivery date
                            </th>

                            <th>Price(CAD)
                            </th>
                        </tr>
                </HeaderTemplate>

                <ItemTemplate>

                    <tr>
                        <td>
                            <asp:Label runat="server" ID="ServiceName" Text='<%# Eval("ServiceName") %>' />
                        </td>

                        <td>
                            <asp:Label runat="server" ID="DeliveryDate" Text='<%# Eval("DeliveryDate") %>' />
                        </td>

                        <td>
                            <asp:Label runat="server" ID="Price" Text='<%# Eval("Price") %>' />
                        </td>
                    </tr>

                </ItemTemplate>

                <FooterTemplate>
                    </table>

                </FooterTemplate>

            </asp:Repeater>
        </div>

        <footer>
            <p>&copy; <%: DateTime.Now.Year %> - UPS Rating API Sample</p>
        </footer>
    </form>
</body>
</html>
