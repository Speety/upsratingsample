﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using UPSRatingSample.Models;

namespace UPSRatingSample.Pages
{
    public partial class ResultPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["rateCard"] != null)
                {
                    List<ServiceRatingDTO> rateCard = Session["rateCard"] as List<ServiceRatingDTO>;
                    dataGrid.DataSource = rateCard;
                    dataGrid.DataBind();
                }
            }
        }
    }
}