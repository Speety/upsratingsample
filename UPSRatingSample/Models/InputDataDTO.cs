﻿namespace UPSRatingSample.Models
{
    public class InputDataDTO
    {
        //Ship from
        public string ShipFromAddress { get; set; }

        public string ShipFromCity { get; set; }

        public string ShipFromCountryCode { get; set; }

        public string ShipFromStateProvinceCode { get; set; }

        public string ShipFromStatePostalCode { get; set; }

        //Ship to
        public string ShipToAddress { get; set; }

        public string ShipToCity { get; set; }

        public string ShipToCountryCode { get; set; }

        public string ShipToStateProvinceCode { get; set; }

        public string ShipToStatePostalCode { get; set; }
    }

    public class ServiceRatingDTO
    {
        public string ServiceName { get; set; }
        public string DeliveryDate { get; set; }
        public string Price { get; set; }
    }
}