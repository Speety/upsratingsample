﻿namespace UPSRatingSample.Models
{
    public enum ServiceNameEnum
    {
        NextDayAir = 1,
        SecondDayAir = 2,
        Ground = 4,
        WorldwideExpress = 8,
        WorldwideExpedited = 16,
        Standard = 32,
        ThreeDaySelect = 64,
        NextDayAirSaver = 128,
        NextDayAirEarlyAM = 256,
        WorldwideExpressPlus = 512,
        SecondDayAirAM = 1024,
        ExpressSaver = 2048,
        SurePost = 4096,
        All = 8191
    }
}