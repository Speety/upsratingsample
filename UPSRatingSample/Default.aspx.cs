﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using UPSRatingSample.Models;
using UPSRatingSample.UPSRateWebReference;

namespace UPSRatingSample
{
    public partial class _Default : Page
    {
        //Get cridentials from web.config
        string id = WebConfigurationManager.AppSettings["Id"];
        string password = WebConfigurationManager.AppSettings["Password"];
        string licensNumber = WebConfigurationManager.AppSettings["LicensNumber"];

        RateService rate = new RateService();
        RateRequest rateRequest = new RateRequest();
        private readonly Hashtable serviceCodes = new Hashtable(12);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Ship from/to country codes
                ListItem itemCA = new ListItem("CA", "CA");
                ListItem itemUS = new ListItem("US", "US");
                this.ShipFromCountryCode.Items.Add(itemCA);
                this.ShipFromCountryCode.Items.Add(itemUS);
                this.ShipToCountryCode.Items.Add(itemCA);
                this.ShipToCountryCode.Items.Add(itemUS);

                //Populate ProvinceState ShipTo / ShipFrom
                this.PopulateProvinceState("CA", "ShipFrom");
                this.PopulateProvinceState("CA", "ShipTo");
            }
        }

        protected void ddlShipFromCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //repopulate provicne state when new ShipToContry is selected
            var selectedCountry = ShipFromCountryCode.Text;
            this.PopulateProvinceState(selectedCountry, "ShipFrom");
        }

        protected void ddlShipToCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //repopulate provicne state when new ShipToContry is selected
            var selectedCountry = ShipToCountryCode.Text;
            this.PopulateProvinceState(selectedCountry, "ShipTo");
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                //Init UPS service
                this.InitService(id, password, licensNumber);

                InputDataDTO inputDataDTO = new InputDataDTO()
                {
                    //Ship from
                    ShipFromAddress = this.ShipFromAddress.Text,
                    ShipFromCity = this.ShipFromCity.Text,
                    ShipFromCountryCode = this.ShipFromCountryCode.Text,
                    ShipFromStatePostalCode = this.ShipFromStatePostalCode.Text,
                    ShipFromStateProvinceCode = this.ShipFromStateProvinceCode.Text,

                    //Ship to
                    ShipToAddress = this.ShipToAddress.Text,
                    ShipToCity = this.ShipToCity.Text,
                    ShipToCountryCode = this.ShipToCountryCode.Text,
                    ShipToStatePostalCode = this.ShipToStatePostalCode.Text,
                    ShipToStateProvinceCode = this.ShipToStateProvinceCode.Text
                };

                var rateCard = this.GetRateCard(inputDataDTO);

                Session["rateCard"] = rateCard;

                Response.Redirect("/Pages/ResultPage.aspx");
            }
        }

        #region methods

        private void PopulateProvinceState(string selectedCountry, string targetControl)
        {
            DropDownList listToPopulate;

            if (targetControl == "ShipFrom")
            {
                //Select ShipFromStateProvinceCode dropdownlist
                listToPopulate = this.ShipFromStateProvinceCode;
            }
            else
            {
                //Select ShipToStateProvinceCode dropdownlist
                listToPopulate = this.ShipToStateProvinceCode;
            }

            listToPopulate.Items.Clear();

            if (selectedCountry == "CA")
            {
                foreach (var province in this.ProvinceCodesList)
                {
                    ListItem item = new ListItem(province, province);
                    listToPopulate.Items.Add(item);
                }
            }
            else if (selectedCountry == "US")
            {
                foreach (var state in this.StateCodesList)
                {
                    ListItem item = new ListItem(state, state);
                    listToPopulate.Items.Add(item);
                }
            }
            else
            {
                //wrong inpput
            }
        }

        private List<string> StateCodesList = new List<string>()
        {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "DC",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"
        };

        private List<string> ProvinceCodesList = new List<string>()
        {
            "NL",
            "PE",
            "NS",
            "NB",
            "QC",
            "ON",
            "MB",
            "SK",
            "AB",
            "BC",
            "YT",
            "NT",
            "NU"
        };

        #endregion

        #region service

        public void InitService(string id, string password, string licensNumber)
        {
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(licensNumber))
            {
                this.rate = new RateService();
                this.rateRequest = new RateRequest();
                RequestType request = new RequestType();
                String[] requestOption = { "ShopTimeInTransit" };
                request.RequestOption = requestOption;
                request.TransactionReference = new TransactionReferenceType()
                {
                    CustomerContext = "Rating and Service",
                };
                rateRequest.Request = request;
                rateRequest.CustomerClassification = new CodeDescriptionType()
                {
                    Code = "01"
                };
                rateRequest.PickupType = new CodeDescriptionType()
                {
                    Code = "03"
                };

                this.SetCredantials(id, password, licensNumber);
                this.LoadServiceCodes();
            }
        }

        private void SetCredantials(string id, string password, string licensNumber)
        {
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upsSvcToken = new UPSSecurityServiceAccessToken();
            upsSvcToken.AccessLicenseNumber = licensNumber;
            upss.ServiceAccessToken = upsSvcToken;
            UPSSecurityUsernameToken upsSecUsrnameToken = new UPSSecurityUsernameToken();
            upsSecUsrnameToken.Username = id;
            upsSecUsrnameToken.Password = password;
            upss.UsernameToken = upsSecUsrnameToken;
            this.rate.UPSSecurityValue = upss;
        }

        public List<ServiceRatingDTO> GetRateCard(InputDataDTO inputDataDTO)
        {
            try
            {
                List<ServiceRatingDTO> rateCard = new List<ServiceRatingDTO>();

                //ship from
                ShipFromType shipFrom = new ShipFromType();
                ShipAddressType shipFromAddress = new ShipAddressType();
                shipFromAddress.City = inputDataDTO.ShipFromCity;
                shipFromAddress.PostalCode = inputDataDTO.ShipFromStatePostalCode;
                shipFromAddress.StateProvinceCode = inputDataDTO.ShipFromStateProvinceCode;
                shipFromAddress.CountryCode = inputDataDTO.ShipFromCountryCode;
                shipFrom.Address = shipFromAddress;

                //ship to
                ShipToType shipTo = new ShipToType();
                ShipToAddressType shipToAddress = new ShipToAddressType();
                shipToAddress.City = inputDataDTO.ShipToCity;
                shipToAddress.PostalCode = inputDataDTO.ShipToStatePostalCode;
                shipToAddress.StateProvinceCode = inputDataDTO.ShipToStateProvinceCode;
                shipToAddress.CountryCode = inputDataDTO.ShipToCountryCode;
                shipTo.Address = shipToAddress;

                //Generate default pachage
                var package = this.GetDefPackage();

                this.rateRequest.Shipment = new ShipmentType
                {
                    Shipper = new ShipperType()
                    {
                        Address = shipFromAddress
                    },
                    ShipFrom = shipFrom,
                    ShipTo = shipTo,
                    Package = package,
                    DeliveryTimeInformation = new TimeInTransitRequestType
                    {
                        PackageBillType = "07"
                    },
                    ShipmentTotalWeight = new ShipmentWeightType
                    {
                        Weight = "100",
                        UnitOfMeasurement = new CodeDescriptionType
                        {
                            Code = "LBS"
                        }
                    }
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11; //This line will ensure the latest security protocol for consuming the web service call.
                RateResponse rateResponse = rate.ProcessRate(rateRequest);

                if (rateResponse.Response.ResponseStatus.Description == "Success")
                {
                    foreach (var rate in rateResponse.RatedShipment)
                    {
                        var rating = new ServiceRatingDTO()
                        {
                            Price = rate.TotalCharges.MonetaryValue,
                            ServiceName = this.serviceCodes[rate.Service.Code].ToString(),
                        };

                        if (rate.TimeInTransit != null)
                        {
                            if (!string.IsNullOrEmpty(rate.TimeInTransit.ServiceSummary.EstimatedArrival.Arrival.Date))
                            {

                                rating.DeliveryDate = DateTime.ParseExact(rate.TimeInTransit.ServiceSummary.EstimatedArrival.Arrival.Date, "yyyyMMdd", null).ToString("dd MMMM yyyy");
                            }
                            else
                            {
                                rating.DeliveryDate = "Unknown";
                            }
                        }
                        else
                        {
                            rating.DeliveryDate = "Unknown";
                        }

                        rateCard.Add(rating);
                    }

                    return rateCard;
                }
                else
                {
                    throw new Exception(rateResponse.Response.ResponseStatus.Description);
                }
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Console.WriteLine("");
                Console.WriteLine("---------Freight Rate Web Service returns error----------------");
                Console.WriteLine("---------\"Hard\" is user error \"Transient\" is system error----------------");
                Console.WriteLine("SoapException Message= " + ex.Message);
                Console.WriteLine("");
                Console.WriteLine("SoapException Category:Code:Message= " + ex.Detail.LastChild.InnerText);
                Console.WriteLine("");
                Console.WriteLine("SoapException XML String for all= " + ex.Detail.LastChild.OuterXml);
                Console.WriteLine("");
                Console.WriteLine("SoapException StackTrace= " + ex.StackTrace);
                Console.WriteLine("-------------------------");
                Console.WriteLine("");
            }
            catch (Exception ex)
            {
                Console.WriteLine("");
                Console.WriteLine("-------------------------");
                Console.WriteLine(" General Exception= " + ex.Message);
                Console.WriteLine(" General Exception-StackTrace= " + ex.StackTrace);
                Console.WriteLine("-------------------------");
            }
            finally
            {
            }

            return null;
        }

        private void LoadServiceCodes()
        {
            this.serviceCodes.Add("01", new AvailableService("UPS Next Day Air", 1));
            this.serviceCodes.Add("02", new AvailableService("UPS Second Day Air", 2));
            this.serviceCodes.Add("03", new AvailableService("UPS Ground", 4));
            this.serviceCodes.Add("07", new AvailableService("UPS Worldwide Express", 8));
            this.serviceCodes.Add("08", new AvailableService("UPS Worldwide Expedited", 16));
            this.serviceCodes.Add("11", new AvailableService("UPS Standard", 32));
            this.serviceCodes.Add("12", new AvailableService("UPS 3-Day Select", 64));
            this.serviceCodes.Add("13", new AvailableService("UPS Next Day Air Saver", 128));
            this.serviceCodes.Add("14", new AvailableService("UPS Next Day Air Early AM", 256));
            this.serviceCodes.Add("54", new AvailableService("UPS Worldwide Express Plus", 512));
            this.serviceCodes.Add("59", new AvailableService("UPS 2nd Day Air AM", 1024));
            this.serviceCodes.Add("65", new AvailableService("UPS Express Saver", 2048));
            this.serviceCodes.Add("93", new AvailableService("UPS Sure Post", 4096));
        }

        private PackageType[] GetDefPackage()
        {
            PackageType package = new PackageType()
            {
                Dimensions = new DimensionsType()
                {
                    Length = "5",
                    Height = "4",
                    Width = "3",
                    UnitOfMeasurement = new CodeDescriptionType
                    {
                        Code = "IN"
                    }
                },
                PackagingType = new CodeDescriptionType
                {
                    Code = "02"
                },
                PackageWeight = new PackageWeightType
                {
                    Weight = "100",
                    UnitOfMeasurement = new CodeDescriptionType()
                    {
                        Code = "LBS"
                    }
                }
            };

            PackageType[] pkgArray = { package };

            return pkgArray;
        }

        private struct AvailableService
        {
            public readonly int EnumValue;
            public readonly string Name;

            public AvailableService(string name, int enumValue)
            {
                Name = name;
                EnumValue = enumValue;
            }

            public override string ToString()
            {
                return Name;
            }
        }

        #endregion
    }
}