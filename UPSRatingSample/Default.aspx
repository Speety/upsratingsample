﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UPSRatingSample._Default" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - UPS Rating API Sample</title>
    <link href="~/Content/Site.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <form runat="server">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" runat="server" href="~/">UPS Rating API Sample</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a runat="server" href="~/">Home</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container body-content">
            <div class="form-horizontal">
                <h4>Please provide shipping info</h4>
                <hr />

                <div class="col-md-6">
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipFromAddress" CssClass="col-md-4 control-label">Ship from address</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="ShipFromAddress" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipFromAddress"
                                CssClass="validation-text text-danger" ErrorMessage="Ship from address field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipFromCity" CssClass="col-md-4 control-label">Ship from city</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="ShipFromCity" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="ShipFromCity"
                                CssClass="validation-text text-danger" ErrorMessage="Ship from city field is required." />
                            <asp:RegularExpressionValidator runat="server" CssClass="validation-text text-danger" ControlToValidate="ShipFromCity" ErrorMessage="Field should contain letters only"
                                ValidationExpression="^[A-Za-z]+$">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipFromCountryCode" CssClass="col-md-4 control-label">Ship from country</asp:Label>
                        <div class="col-md-8">
                            <asp:DropDownList runat="server" ID="ShipFromCountryCode" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlShipFromCountry_SelectedIndexChanged" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipFromCountryCode"
                                CssClass="validation-text text-danger" ErrorMessage="Ship from country field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipFromStateProvinceCode" CssClass="col-md-4 control-label">Ship from state</asp:Label>
                        <div class="col-md-8">
                            <asp:DropDownList runat="server" ID="ShipFromStateProvinceCode" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipFromStateProvinceCode"
                                CssClass="validation-text text-danger" ErrorMessage="Ship from state field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipFromStatePostalCode" CssClass="col-md-4 control-label">Ship from postal</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="ShipFromStatePostalCode" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipFromStatePostalCode"
                                CssClass="validation-text text-danger" ErrorMessage="Ship from postal field is required." />
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipToAddress" CssClass="col-md-4 control-label">Ship to adress</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="ShipToAddress" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipToAddress"
                                CssClass="validation-text text-danger" ErrorMessage="Ship to adress field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipToCity" CssClass="col-md-4 control-label">Ship to city</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="ShipToCity" CssClass="form-control" />
                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ControlToValidate="ShipToCity"
                                CssClass="validation-text text-danger" ErrorMessage="Ship to city field is required." />
                            <asp:RegularExpressionValidator runat="server" CssClass="validation-text text-danger" ControlToValidate="ShipFromCity" ErrorMessage="Field should contain letters only"
                                ValidationExpression="^[A-Za-z]+$">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipToCountryCode" CssClass="col-md-4 control-label">Ship to country</asp:Label>
                        <div class="col-md-8">
                            <asp:DropDownList runat="server" ID="ShipToCountryCode" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlShipToCountry_SelectedIndexChanged" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipToCountryCode"
                                CssClass="validation-text text-danger" ErrorMessage="Ship to country field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipToStateProvinceCode" CssClass="col-md-4 control-label">Ship to state</asp:Label>
                        <div class="col-md-8">
                            <asp:DropDownList runat="server" ID="ShipToStateProvinceCode" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipToStateProvinceCode"
                                CssClass="validation-text text-danger" ErrorMessage="Ship to state field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ShipToStatePostalCode" CssClass="col-md-4 control-label">Ship to postal</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="ShipToStatePostalCode" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ShipToStatePostalCode"
                                CssClass="validation-text text-danger" ErrorMessage="Ship to postal field is required." />
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group" style="text-align: center;">
                        <asp:Button CssClass="btn btn-primary col-md-12 SubmitButton"
                            Text="Submit"
                            OnClick="SubmitButton_Click"
                            runat="server" />
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <p>&copy; <%: DateTime.Now.Year %> - UPS Rating API Sample</p>
        </footer>
    </form>
</body>
</html>
